<?php

/**
 * Implements hook_field_info().
 */
function ncif_field_info(){
  return array(
    'ncif_location' => array(
      'label' => t('Named Address'),
      'description' => t('Allows adding a postal address and a geographical location with a name.'),
      'settings' => array(),
      'instance_settings' => array(),
      'default_widget' => 'ncif_text_widget',
      'default_formatter' => 'ncif_field_formatter',
    ),
    'ncif_email' => array(
      'label' => t('Named Email'),
      'description' => t('Allows adding an email address with a name'),
      'settings' => array(),
      'instance_settings' => array(),
      'default_widget' => 'ncif_text_widget',
      'default_formatter' => 'ncif_field_formatter',
    ),
    'ncif_phone' => array(
      'label' => t('Named Phone'),
      'description' => t('Allows adding a phone number with a name'),
      'settings' => array(),
      'instance_settings' => array(),
      'default_widget' => 'ncif_text_widget',
      'default_formatter' => 'ncif_field_formatter',
    ),
  );
}

function ncif_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  //TODO: Do we need to validate here?  Or are we going to validate in the widget.
}

function ncif_field_is_empty($item, $field) {
  $type = $field['type'];
  if($type == 'ncif_location'){
    return empty($item['address1']) && empty($item['address2']) && empty($item['city']) && empty($item['zip']);
  }
  else{
    return (empty($item['value']) && (string) $item['value'] !== '0');
  }
}

/**
 * Implements hook_field_widget_info().
 */
function ncif_field_widget_info(){
  return array(
    'ncif_text_widget' => array(
      'label' => t('Text field and dropdown'),
      'field types' => array('ncif_email', 'ncif_phone', 'ncif_location'),
      'settings' => array(), 
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT, 
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ), 
  );
}

/**
 * Implements hook_field_widget_form().
 */
function ncif_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $type = $instance['type'];
  $element += array(
    '#title' => check_plain($instance['label']),
    '#label' => $instance['label'],
  );
  $element['name'] = array(
    '#type' => 'select',
    '#options' => variable_get($type . '_names', array()),
    '#default_value' => isset($items[$delta]['name']) ? $items[$delta]['name']:'',
  );
  switch($type){
    case 'ncif_email':
    case 'ncif_phone':
      $element['value'] = array(
        '#type' => 'textfield',
        '#maxlength' => 255,
        '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value']:'',
        '#element_validate' => array($type . '_element_validate'),
      );
    break;
    case 'ncif_location':
      $element['address1'] = array(
          '#type' => 'textfield',
          '#maxlength' => 255,
          '#title' => t('Line 1'),
          '#default_value' => isset($items[$delta]['address1']) ? $items[$delta]['address1']:'',
      );
      $element['address2'] = array(
          '#type' => 'textfield',
          '#maxlength' => 255,
          '#title' => t('Line 2'),
          '#default_value' => isset($items[$delta]['address2']) ? $items[$delta]['address2']:'',
      );
      $element['city'] = array(
          '#type' => 'textfield',
          '#maxlength' => 255,
          '#title' => t('City'),
          '#default_value' => isset($items[$delta]['city']) ? $items[$delta]['city']:'',
      );
      $element['state'] = array(
          '#type' => 'select',
          '#options' => ncif_state_abbreviations(),
          '#title' => t('State'),
          '#default_value' => isset($items[$delta]['state']) ? $items[$delta]['state']:'',
      );
      $element['zip'] = array(
          '#type' => 'textfield',
          '#maxlength' => 10,
          '#size' => 10,
          '#title' => t('Zip'),
          '#default_value' => isset($items[$delta]['zip']) ? $items[$delta]['zip']:'',
      );
    break;
  }
  return $element;
}

function ncif_email_element_validate($element, &$form_state, $form) {
  if((!empty($element['#value'])) && (!valid_email_address($element['#value']))){
    form_error($element, t('Invalid email address.'));
  }
}

function ncif_phone_element_validate($element, &$form_state, $form) {
  //TODO:  Add phone validation code
  
}

/**
 * Implements hook_field_formatter_info().
 */
function ncif_field_formatter_info(){
  return array(
    'ncif_field_formatter' => array(
      'label' => t('HF Default'),
      'field types' => array('ncif_email', 'ncif_phone', 'ncif_location'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function ncif_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $type = $field['type'];
  $names = variable_get($type . '_names', array());
  $elements = array();
  foreach($items as $delta=>$item){
    $item['name'] = isset($names[$item['name']]) ? $names[$item['name']] : $item['name'];
    $item['type'] = $type;
    $elements[$delta] = array(
      '#markup' => theme(array("ncif_name__$type", 'ncif_name'), $item) . theme(array($type, 'ncif_value'), $item),
    );
  }
  return $elements;
}

/**
 * Implements hook_theme().
 */
function ncif_theme(){
  return array(
    'ncif_name' => array(
      'variables' => array('name'=>NULL,'type'=>NULL),
    ),
    'ncif_value' => array(
        'variables' => array('name'=>NULL, 'type'=>NULL),
    ),
    'ncif_email' => array(
        'variables' => array('name'=>NULL, 'type'=>NULL),
    ),
  );
}

function theme_ncif_name($variables) {
  $name = $variables['name'];
  $type = drupal_html_class($variables['type']);
  return "<span class=\"ncif-name ncif-name-$type\">$name:</span> ";
}

function theme_ncif_value($variables) {
  $name = $variables['name'];
  unset($variables['name']);
  $type = drupal_html_class($variables['type']);
  unset($variables['type']);
  $output = "<div class=\"ncif-value ncif-value-$type\">";
  foreach($variables as $key=>$value){
    if(!empty($value)){
      $output .= "<span class=\"ncif-value-part ncif-value-part-$key\">$value</span> ";
    }
  }
  $output .= '</div>';
  return $output;
}

function theme_ncif_email($variables){
  $type = drupal_html_class($variables['type']);
  $name = $variables['name'];
  $value = $variables['value'];
  $output = "<div class=\"ncif-value ncif-value-$type\">";
  if(!empty($value)){
    $value = l($value, "mailto:$value");
    $output .= "<span class=\"ncif-value-part ncif-value-part-value\">$value</span> ";
  }
  $output .= '</div>';
  return $output;
}

function ncif_state_abbreviations() {
   return array( 
       'AK' => 'AK', 'AL' => 'AL', 'AR' => 'AR', 'AZ' => 'AZ', 'CA' => 'CA', 
       'CO' => 'CO', 'CT' => 'CT', 'DC' => 'DC', 'DE' => 'DE', 'FL' => 'FL', 
       'GA' => 'GA', 'HI' => 'HI', 'IA' => 'IA', 'ID' => 'ID', 'IL' => 'IL', 
       'IN' => 'IN', 'KS' => 'KS', 'KY' => 'KY', 'LA' => 'LA', 'MA' => 'MA', 
       'MD' => 'MD', 'ME' => 'ME', 'MI' => 'MI', 'MN' => 'MN', 'MO' => 'MO', 
       'MS' => 'MS', 'MT' => 'MT', 'NC' => 'NC', 'ND' => 'ND', 'NE' => 'NE',  
       'NH' => 'NH', 'NJ' => 'NJ', 'NM' => 'NM', 'NV' => 'NV', 'NY' => 'NY', 
       'OH' => 'OH', 'OK' => 'OK', 'OR' => 'OR', 'PA' => 'PA', 'RI' => 'RI', 
       'SC' => 'SC', 'SD' => 'SD', 'TN' => 'TN', 'TX' => 'TX', 'UT' => 'UT', 
       'VA' => 'VA', 'VT' => 'VT', 'WA' => 'WA', 'WI' => 'WI', 'WV' => 'WV', 
       'WY' => 'WY');
}
